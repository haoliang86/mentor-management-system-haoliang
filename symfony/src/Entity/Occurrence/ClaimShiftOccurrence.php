<?php


namespace App\Entity\Occurrence;


use App\Entity\User\User;
use App\Utils\DateTimeService;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @package App\Entity\Occurrence
 */
class ClaimShiftOccurrence extends AttendanceOccurrence
{
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $shiftDateTime;

    public function __construct(User $subject, \DateTime $shiftDateTime, float $points = null,
                                string $adminNotes = null, DateTimeService $time = null)
    {
        parent::__construct($subject, $points, $adminNotes, $time);
        $this->shiftDateTime = $shiftDateTime;
    }

    /**
     * @return \DateTime
     */
    public function getShiftDateTime()
    {
        return $this->shiftDateTime;
    }
}