<?php

namespace App\Controller;

use App\Entity\Schedule\ScheduledShift;
use App\Entity\Schedule\ShiftAssignment;
use App\Entity\User\User;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * This is a test controller
 * 
 * TODO: Remove this controller before deployment into production
 */
class TestController extends Controller
{
    /**
     * Display mentor's schedule as JSON (all shifts in the database assigned to the mentor)
     * 
     * @Route("/test_schedule/{username}", name="test_schedule")
     */
    public function getMentorSchedule(Request $request, User $user)
    {
        $mentorShiftAsignments = $this->getDoctrine()
            ->getRepository(ShiftAssignment::class)
            ->findByMentor($user);

        $mentorSchedule = array_map(function ($shiftAssignment) {
            $scheduledShift = $shiftAssignment->getScheduledShift();
            $shiftInfo = $scheduledShift->getShift();
            
            $mentorScheduleEntry = array();
            $mentorScheduleEntry["date"] = $scheduledShift->getDate()->format('m-d-Y');
            $mentorScheduleEntry["day"] = $shiftInfo->getDay();
            $mentorScheduleEntry["time"] = $shiftInfo->getStartTime()->format('H:i:s');
            
            // If a shift assignment has no subject, then the assigned mentor is the shift leader
            $mentorScheduleEntry["subject"] = $shiftAssignment->getSubject()
                ? $shiftAssignment->getSubject()->getName()
                : "Shift Leader";
            
            return $mentorScheduleEntry;
        }, $mentorShiftAsignments);

        $response = new JsonResponse($mentorSchedule, 200);

        // This line is just to make it more human-readable in browser; can safely delete
        $response->setEncodingOptions(JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);

        return $response;
    }
}