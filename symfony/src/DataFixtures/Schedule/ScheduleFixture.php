<?php

namespace App\DataFixtures\Schedule;

use App\DataFixtures\Misc\RoomFixture;
use App\DataFixtures\Misc\SemesterFixture;
use App\DataFixtures\Misc\SubjectFixture;
use App\DataFixtures\User\UserFixture;
use App\Entity\Schedule\Schedule;
use App\Entity\Schedule\ScheduledShift;
use App\Entity\Schedule\Shift;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;

class ScheduleFixture extends Fixture implements DependentFixtureInterface {
    const SCHEDULE = 'schedule';
    public $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    public function load(ObjectManager $manager) {
        $semester = $this->getReference(SemesterFixture::ACTIVE);
        $room = $this->getReference(RoomFixture::ECSS_4415);

        $schedule = new Schedule($semester);
        $manager->persist($schedule);

        $mentors = array();

        for ($i = 0; $i < 30; $i++) {
            $p = str_pad($i, 6, '0', STR_PAD_LEFT);
            $mentors[$i] = $this->getReference(UserFixture::MENTOR . $p);
        }

        $subjects = array(
            array(
                'subject' => $this->getReference(SubjectFixture::JAVA),
                'max' => 3
            ),
            array(
                'subject' => $this->getReference(SubjectFixture::CPP),
                'max' => 3
            ),
            array(
                'subject' => $this->getReference(SubjectFixture::DISCRETE_MATH),
                'max' => 3
            ),
            array(
                'subject' => $this->getReference(SubjectFixture::COMPUTER_ARCHITECTURE),
                'max' => 3
            )
        );

        // create shifts
        $shifts = array();

        $start = new \DateTime('10am');
        $end = new \DateTime('11:30am');

        for ($day = 1; $day < 6; $day++) {
            $shifts[$day] = array();
            $s = $start;
            $e = $end;
            for ($i = 0; $i < 8; $i++) {
                if ($i == 0) {
                    $s = new \DateTime($start->format('H:i'));
                    $e = new \DateTime($end->format('H:i'));
                } elseif ($i == 4 && $day == 5) {
                    $s = new \DateTime($e->format('H:i'));
                    $e = new \DateTime($e->format('H:i'));
                    $e->add(new \DateInterval('PT2H'));
                } elseif ($i == 7 && ($day == 1 || $day == 4)) {
                    continue;
                } elseif ($i > 3 && $day == 5) {
                    continue;
                } else {
                    $s = new \DateTime($e->format('H:i'));
                    $e = new \DateTime($e->format('H:i'));
                    $e->add(new \DateInterval('PT1H30M'));
                }

                $shift = new Shift($schedule, $room, $s, $e, $day);

                foreach ($subjects as $subject) {
                    $shift->addSubject($subject['subject'], $subject['max']);
                }

                // pick 9 mentors for this shift (2 per subject, plus 1 shift leader)
                $mentorIndices = range(0, 29);
                shuffle($mentorIndices);
                $shiftMentorIndices = array_slice($mentorIndices, 0, 9);
                $shiftMentors = array();
                for ($mentorIndex = 0; $mentorIndex < 9; $mentorIndex++) {
                    $shiftMentors[$mentorIndex] = $mentors[$shiftMentorIndices[$mentorIndex]];
                }

                $shift->assignShiftLeader($shiftMentors[0]);

                $mentorIndex = 1;
                foreach ($subjects as $subject) {
                    $shift->addMentor($subject['subject'], $shiftMentors[$mentorIndex]);
                    $shift->addMentor($subject['subject'], $shiftMentors[$mentorIndex + 1]);
                    $mentorIndex += 2;
                }

                $manager->persist($shift);

                $shifts[$day][] = $shift;
            }
        }

        $start->setTime(12, 0, 0);
        $end->setTime(2, 0, 0);

        $shifts[0] = array();
        $shifts[6] = array();
        for ($i = 0; $i < 3; $i++) {
            $s = new \DateTime($start->format('H:i'));
            $s->add(new \DateInterval('PT' . ($i * 2) . 'H'));
            $e = new \DateTime($start->format('H:i'));
            $e->add(new \DateInterval('PT' . ($i * 2) . 'H'));

            $shift_sun = new Shift($schedule, $room, $s, $e, 0);
            $shift_sat = new Shift($schedule, $room, $s, $e, 6);

            // pick 9 mentors for this shift (2 per subject, plus 1 shift leader)
            $mentorIndices = range(0, 29);
            shuffle($mentorIndices);
            $shiftMentorIndices = array_slice($mentorIndices, 0, 9);
            $shiftMentors = array();
            for ($mentorIndex = 0; $mentorIndex < 9; $mentorIndex++) {
                $shiftMentors[$mentorIndex] = $mentors[$shiftMentorIndices[$mentorIndex]];
            }

            $shift_sun->assignShiftLeader($shiftMentors[0]);
            $shift_sat->assignShiftLeader($shiftMentors[0]);

            $mentorIndex = 1;
            foreach ($subjects as $subject) {
                $shift_sun->addSubject($subject['subject'], $subject['max']);
                $shift_sun->addMentor($subject['subject'], $shiftMentors[$mentorIndex]);
                $shift_sun->addMentor($subject['subject'], $shiftMentors[$mentorIndex + 1]);
                $mentorIndex += 2;
            }

            $mentorIndex = 1;
            foreach ($subjects as $subject) {
                $shift_sat->addSubject($subject['subject'], $subject['max']);
                $shift_sat->addMentor($subject['subject'], $shiftMentors[$mentorIndex]);
                $shift_sat->addMentor($subject['subject'], $shiftMentors[$mentorIndex + 1]);
                $mentorIndex += 2;
            }

            $manager->persist($shift_sun);
            $manager->persist($shift_sat);

            $shifts[0][] = $shift_sun;
            $shifts[6][] = $shift_sat;
        }

        // generate scheduled shifts and assignments
        $start_date = $semester->getStartDate();
        $end_date = $semester->getEndDate();
        $interval = new \DateInterval('P1D');

        $period = new \DatePeriod($start_date, $interval, $end_date);

        foreach ($period as $date) {
            $day = $date->format('w');

            $s = $shifts[$day];

            foreach ($s as $shift) {
                $scheduled_shift = new ScheduledShift($schedule, $shift, $date);

                $manager->persist($scheduled_shift);
            }
        }

        $manager->flush();

        $this->addReference(self::SCHEDULE, $schedule);
    }

    public function getDependencies() {
        return [
            SemesterFixture::class,
            RoomFixture::class,
            SubjectFixture::class,
            UserFixture::class
        ];
    }
}