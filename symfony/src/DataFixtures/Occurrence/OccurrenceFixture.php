<?php


namespace App\DataFixtures\Occurrence;


use App\DataFixtures\User\UserFixture;
use App\Entity\Occurrence\AbsenceOccurrence;
use App\Entity\Occurrence\BehaviorOccurrence;
use App\Entity\Occurrence\CumulativeTardinessOccurrence;
use App\Entity\Occurrence\Occurrence;
use App\Entity\Occurrence\TardinessOccurrence;
use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class OccurrenceFixture extends Fixture implements DependentFixtureInterface
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        for ($n = 0; $n < UserFixture::MENTOR_AMOUNT; $n++) {
            $p = str_pad($n, 6, '0', STR_PAD_LEFT);
            /** @var User $user */
            $user = $this->getReference(UserFixture::MENTOR . $p);
            $this->addRandomBehaviorOccurrences($user, Occurrence::STATUS_PENDING);
            $this->addRandomBehaviorOccurrences($user, Occurrence::STATUS_APPROVED);
            $this->addRandomBehaviorOccurrences($user, Occurrence::STATUS_REJECTED);
            $this->addRandomAbsence($user, Occurrence::STATUS_PENDING);
            $this->addRandomAbsence($user, Occurrence::STATUS_REJECTED);
            $this->addRandomAbsence($user, Occurrence::STATUS_APPROVED);
            $this->addRandomCumulativeTardiness($user);
            $this->addRandomTardiness($user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getDependencies()
    {
        return array(
            UserFixture::class
        );
    }

    private function addRandomTardiness(User $user) {
        $shouldCreate = rand(0, 2);
        if ($shouldCreate === 1) {
            $occurrence = new TardinessOccurrence($user, -1, 15);

            // Decide status
            switch (rand(0, 2)) {
                case 0:
                    $occurrence->approve();
                    break;
                case 1:
                    $occurrence->reject();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param User $user
     * @param string $status
     */
    private function addRandomBehaviorOccurrences(User $user, string $status)
    {
        $limit = rand(0, 3);
        for ($i = 0; $i < $limit; $i++) {
            $occurrence = new BehaviorOccurrence($user, 'Something', 'Details', new \DateTime());

            if ($status == Occurrence::STATUS_APPROVED) {
                $occurrence->approve();
            } elseif ($status == Occurrence::STATUS_REJECTED) {
                $occurrence->reject();
            }
        }
    }

    private function addRandomAbsence(User $user, string $status)
    {
        $randNoticeGiven = rand(0, 1) === 0;

        if ($randNoticeGiven) {
            $occurrence = new AbsenceOccurrence($user, new \DateTime('10:00:00'), AbsenceOccurrence::NO_NOTICE, -5);
            if ($status == Occurrence::STATUS_APPROVED) {
                $occurrence->approve();
            } elseif ($status == Occurrence::STATUS_REJECTED) {
                $occurrence->reject();
            }
        } else {
            $hoursNotice = rand(0, 36);
            $occurrence = new AbsenceOccurrence($user, new \DateTime('10:00:00'), $hoursNotice, $hoursNotice < 24 ? -4 : -3);
            if ($status == Occurrence::STATUS_APPROVED) {
                $occurrence->approve();
            } elseif ($status == Occurrence::STATUS_REJECTED) {
                $occurrence->reject();
            }
        }

    }

    private function addRandomCumulativeTardiness(User $user)
    {
        $limit = rand(0, 3);

        for ($i = 0; $i < $limit; $i++) {
            $accumulatedOccurrences = array();
            // Accumulated occurrences
            for ($j = 0; $j < 5; $j++) {
                // Accumulated ones have zero points
                $newAccumulated = new TardinessOccurrence($user, 0, 1);
                $accumulatedOccurrences[] = $newAccumulated;
            }
            $cumulativeOccurrence = new CumulativeTardinessOccurrence($user, -rand(0, 3), 5,
                new \DateTime('yesterday'), new \DateTime(), $accumulatedOccurrences);

            // Decide status
            switch (rand(0, 2)) {
                case 0:
                    $cumulativeOccurrence->approve();
                    break;
                case 1:
                    $cumulativeOccurrence->reject();
                    break;
                default:
                    break;
            }
        }
    }
}